Easy auto versioning of gitlab projects using continous integration and a clean workflow.

# assumptions

There are a few assumptions on the project structure and your knowledge. We're assuming you have the following:

1. knowledge of [SemVer]()
2. a permanent branch containing the latest release (default: `main`)
3. a permanent branch containing the next release (default: `next`)
4. preview branches for early access (default: `alpha/beta`)
5. maintenance branches for releases (default: `v{Major}.{Minor}.{Patch}`; eg: `v1.0.0`)
6. feature branches containing small commits used in PR/MR
7. using angular commit message style

We're also assuming you use the following commit header types:

| type | description |
|---|---|
| `ci` | changes to ci configuration |
| `feat` | new feature |
| `fix` | bug fix | 
| `docs` | changes to documentation |
| `style` | changes/improvements to code style |
| `refactor` | code change 

# setup

1. make sure `main` and `development` branches exists
2. include versioning in your `.gitlab-ci.yml`:
   ```yaml
   include: https://gitlab.com/reop/version/main/raw/version.yaml

   stages:
   - versioning
   - ... (build steps)
   ```

# configuration 
Customize versioning by creating `versioning.yml` inside your repository root.

```yaml
branches:
    # main branch with latest release
    main: 'master'
    # dev branch with next release
    next: 'next'
    preview:
    - alpha
    - beta
# safe version to file
output: version.txt
# list of files to parse and replace version (inputFile:outputFile)
artifacts:                      
    # replace version in src/Version.h.in and safe it to src/Version.h
    - src/Version.h.in:src/Version.h
commits:
    header: "{type}({scope}): {summary}"
    body: "{body}"
    footer: "{fixes}\n{closes}"
```

# explaination

The auto-versioning happens inside your gitlab-ci pipelines and can be configured using the `versioning.yml`.

The following table illustrates what happens when triggering the pipelines under specific circumstances:

| branches | actions | example version |
|---|---|---|
| `main`/`next`| <ul><li>generate version (`{Major}.{Minor}.{Patch}`)</li><li>safe version to `version.txt`</li><li>replace version template string in artifact files</li></ul> | `1.0.7` |
| `preview` | <ul><li>generate version (`{Major}.{Minor}.{Patch}-{Preview}`)</li><li>safe version to `version.txt`</li><li>replace version template string in artifact files</li></ul> | `1.0.7-beta` |
| `maintenance` | <ul><li>get version from git tag</li><li>safe version to `version.txt`</li><li>replace version template string in artifact files</li></ul> | `1.0.7` |

## how the version is generated

# usage

After your project is setup and configured gitlab-ci will run versioning when pushing or MR into a specified branch.